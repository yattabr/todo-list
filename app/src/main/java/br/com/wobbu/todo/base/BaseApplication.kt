package br.com.wobbu.todo.base

import android.app.Application
import android.content.Context
import br.com.wobbu.todo.di.AppComponent
import br.com.wobbu.todo.di.AppModule
import br.com.wobbu.todo.di.DaggerAppComponent
import br.com.wobbu.todo.di.UtilsModule

class BaseApplication : Application() {
    lateinit var appComponent: AppComponent
    lateinit var context: Context

    override fun onCreate() {
        super.onCreate()
        context = this
        appComponent =
            DaggerAppComponent.builder().appModule(AppModule(this)).utilsModule(UtilsModule())
                .build()
    }


    protected override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
    }
}