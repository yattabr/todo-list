package br.com.wobbu.todo.data

class Urls {
    companion object {

        const val BASE_URL = "https://bboxx-android-coding-challenge.herokuapp.com/"
        const val POST_TODO = BASE_URL + "todos.json"
        const val GET_TODO = BASE_URL + "todos.json"
        const val PATCH_OR_DELETE_TODO = BASE_URL + "todos/{id}.json"
    }
}