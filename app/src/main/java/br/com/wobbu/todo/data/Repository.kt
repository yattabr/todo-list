package br.com.wobbu.todo.data

import br.com.wobbu.todo.model.Todo
import com.google.gson.JsonElement
import io.reactivex.Observable
import java.util.*


class Repository(private val apiCallInterface: ApiCallInterface) {
    fun fetchTodo(): Observable<JsonElement> {
        return apiCallInterface.fetchTodo()
    }

    fun postTodo(todo: Todo): Observable<JsonElement> {
        return apiCallInterface.postTodo(todo)
    }

    fun updateTodo(todo: Todo): Observable<JsonElement> {
        return apiCallInterface.updateTodo(todo.id, todo)
    }

    fun deleteTodo(todo: Todo): Observable<JsonElement> {
        return apiCallInterface.deleteTodo(todo.id)
    }
}