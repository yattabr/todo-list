package br.com.wobbu.todo.main

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.wobbu.todo.R
import br.com.wobbu.todo.base.BaseApplication
import br.com.wobbu.todo.base.ViewModelFactory
import br.com.wobbu.todo.data.ApiResponse
import br.com.wobbu.todo.data.Status
import br.com.wobbu.todo.model.Todo
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_add_todo.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    var adapter: TodoAdapter? = null
    lateinit var mainViewModel: MainViewModel
    private var todoIdDeleted: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as BaseApplication).appComponent.doInjection(this)
        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        initUI()
        initObserver()


        mainViewModel.fetchTodo()
    }

    private fun initUI() {
        bt_add.setOnClickListener { openAddView() }
        bt_cancel.setOnClickListener { closeAddView() }
        bt_post_todo.setOnClickListener { postOrUpdateTodo() }
        edit_search.addTextChangedListener(filterWatcher)

        val filter_date = arrayOf("Filter by Date", "Last 7 days", "1 month", "6 months")
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, filter_date)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = aa
        spinner.onItemSelectedListener = onItemSelectedListener
    }

    private fun initObserver() {
        mainViewModel.fetchTodoObserver.observe(this, Observer {
            when (it) {
                is ApiResponse -> fetchTodoList(it)
            }
        })
        mainViewModel.postTodoObserver.observe(this, Observer {
            when (it) {
                is ApiResponse -> postResponse(it)
            }
        })
        mainViewModel.updateTodoObserver.observe(this, Observer {
            when (it) {
                is ApiResponse -> updateResponse(it)
            }
        })
        mainViewModel.deleteTodoObserver.observe(this, Observer {
            when (it) {
                is ApiResponse -> deleteResponse()
            }
        })
    }

    private fun fetchTodoList(apiResponse: ApiResponse) {
        when (apiResponse.status) {
            Status.SUCCESS -> {
                val todoList = mainViewModel.convertJsonToTodoList(apiResponse.data.toString())
                mountRecyclerView(todoList)
            }

            Status.LOADING -> {
                loading.visibility = View.VISIBLE
            }

            Status.ERROR -> {
                loading.visibility = View.GONE
                Toast.makeText(this, "Some error happened.", Toast.LENGTH_SHORT).show()
                Log.i("FETCH_ERROR", apiResponse.error.toString())
            }
        }
    }

    private fun postResponse(apiResponse: ApiResponse) {
        when (apiResponse.status) {
            Status.SUCCESS -> {
                cleanAddView()
                loading.visibility = View.GONE
                layout_add_todo.visibility = View.GONE
                bt_add.visibility = View.VISIBLE
                val todo = mainViewModel.convertJsonToTodo(apiResponse.data.toString())
                adapter!!.addTodo(todo)
            }
            Status.LOADING -> {
                loading.visibility = View.VISIBLE
            }
            Status.ERROR -> {
                loading.visibility = View.GONE
                Toast.makeText(this, "Some error happened.", Toast.LENGTH_SHORT).show()
                Log.i("FETCH_ERROR", apiResponse.error.toString())
            }
        }
    }

    private fun updateResponse(apiResponse: ApiResponse) {
        when (apiResponse.status) {
            Status.SUCCESS -> {
                loading.visibility = View.GONE
                layout_add_todo.visibility = View.GONE
                val todo = mainViewModel.convertJsonToTodo(apiResponse.data.toString())
                adapter!!.updateTodo(todo)
            }
            Status.LOADING -> {
                loading.visibility = View.VISIBLE
            }
            Status.ERROR -> {
                loading.visibility = View.GONE
                Toast.makeText(this, "Some error happened.", Toast.LENGTH_SHORT).show()
                Log.i("FETCH_ERROR", apiResponse.error.toString())
            }
        }
    }

    private fun deleteResponse() {
        loading.visibility = View.GONE
        layout_add_todo.visibility = View.GONE
        adapter!!.deleteTodo(todoIdDeleted)
    }

    private fun mountRecyclerView(todos: ArrayList<Todo>) {
        adapter = TodoAdapter(this)
        adapter!!.createTodoList(todos)
        recycler_todo.adapter = adapter
        loading.visibility = View.GONE
    }

    private fun openAddView() {
        layout_add_todo.visibility = View.VISIBLE
        bt_add.visibility = View.GONE
        bt_post_todo.text = getString(R.string.add)
    }

    private fun closeAddView() {
        layout_add_todo.visibility = View.GONE
        bt_add.visibility = View.VISIBLE
    }

    fun openUpdateView(todo: Todo) {
        openAddView()
        edit_add_todo.setText(todo.name)
        edit_add_status.setText(todo.status)
        edit_add_description.setText(todo.description)
        edit_add_expiry.setText(todo.expiry_date)

        bt_post_todo.text = getString(R.string.update)
        bt_post_todo.setOnClickListener { postOrUpdateTodo(true, todo.id) }
    }

    fun completeTodo(todo: Todo) {
        mainViewModel.updateTodo(todo)
    }

    private fun cleanAddView() {
        edit_add_todo.setText("")
        edit_add_status.setText("")
        edit_add_description.setText("")
        edit_add_expiry.setText("")
    }

    fun openDeleteView(todo: Todo) {
        AlertDialog.Builder(this).setTitle("DELETE")
            .setMessage("Are you sure to delete this Todo: ${todo.name}?")
            .setPositiveButton("Yes") { _, _ ->
                mainViewModel.deleteTodo(todo)
                todoIdDeleted = todo.id
            }
            .setNegativeButton("No", null)
            .show()
    }

    private fun postOrUpdateTodo(isUpdate: Boolean = false, id: String = "") {
        hideKeyboard()
        val todo = Todo()
        todo.name = edit_add_todo.text.toString()
        todo.description = edit_add_description.text.toString()
        todo.status = edit_add_status.text.toString()
        todo.expiry_date = edit_add_expiry.text.toString()
        if (todo.name.isNotEmpty() && todo.description.isNotEmpty() && todo.status.isNotEmpty() && todo.expiry_date.isNotEmpty())
            if (!isUpdate) {
                mainViewModel.postTodo(todo)
            } else {
                todo.id = id
                mainViewModel.updateTodo(todo)
            }
        else
            Toast.makeText(this, "You need to fill all fields.", Toast.LENGTH_LONG).show()
    }

    private val filterWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            adapter!!.filter.filter(s)
        }
    }

    private val onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(p0: AdapterView<*>?) {
        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            if (adapter != null) {
                when (p2) {
                    0 -> adapter!!.cleanFilterByDate()
                    1 -> adapter!!.filterBy7Days()
                    2 -> adapter!!.filterBy1Month()
                    3 -> adapter!!.filterBy6Months()
                }
            }
        }

    }


    private fun hideKeyboard() {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }
}