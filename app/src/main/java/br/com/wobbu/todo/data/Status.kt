package br.com.wobbu.todo.data

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}