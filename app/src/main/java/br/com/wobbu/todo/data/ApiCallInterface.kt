package br.com.wobbu.todo.data

import br.com.wobbu.todo.model.Todo
import com.google.gson.JsonElement
import io.reactivex.Observable
import retrofit2.http.*
import java.util.*

interface ApiCallInterface {

    @GET(Urls.GET_TODO)
    fun fetchTodo(): Observable<JsonElement>

    @POST(Urls.POST_TODO)
    fun postTodo(@Body todo: Todo): Observable<JsonElement>

    @PATCH(Urls.PATCH_OR_DELETE_TODO)
    fun updateTodo(@Path("id") id: String, @Body todo: Todo): Observable<JsonElement>

    @DELETE(Urls.PATCH_OR_DELETE_TODO)
    fun deleteTodo(@Path("id") id: String): Observable<JsonElement>
}