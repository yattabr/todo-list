package br.com.wobbu.todo.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.wobbu.todo.data.ApiResponse
import br.com.wobbu.todo.data.Repository
import br.com.wobbu.todo.model.Todo
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel(val repository: Repository) : ViewModel() {

    val fetchTodoObserver = MutableLiveData<Any>()
    val postTodoObserver = MutableLiveData<Any>()
    val updateTodoObserver = MutableLiveData<Any>()
    val deleteTodoObserver = MutableLiveData<Any>()
    private val disposables = CompositeDisposable()

    fun fetchTodo() {
        disposables.add(repository.fetchTodo()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { fetchTodoObserver.setValue(ApiResponse.loading()) }
            .subscribe(
                { result -> fetchTodoObserver.setValue(ApiResponse.success(result)) },
                { throwable -> fetchTodoObserver.setValue(ApiResponse.error(throwable)) }
            ))
    }

    fun postTodo(todo: Todo) {
        disposables.add(repository.postTodo(todo)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { postTodoObserver.setValue(ApiResponse.loading()) }
            .subscribe(
                { result -> postTodoObserver.setValue(ApiResponse.success(result)) },
                { throwable -> postTodoObserver.setValue(ApiResponse.error(throwable)) }
            ))
    }

    fun updateTodo(todo: Todo) {
        disposables.add(repository.updateTodo(todo)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { updateTodoObserver.setValue(ApiResponse.loading()) }
            .subscribe(
                { result -> updateTodoObserver.setValue(ApiResponse.success(result)) },
                { throwable -> updateTodoObserver.setValue(ApiResponse.error(throwable)) }
            ))
    }

    fun deleteTodo(todo: Todo) {
        disposables.add(repository.deleteTodo(todo)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { deleteTodoObserver.setValue(ApiResponse.loading()) }
            .subscribe(
                { result -> deleteTodoObserver.setValue(ApiResponse.success(result)) },
                { throwable -> deleteTodoObserver.setValue(ApiResponse.error(throwable)) }
            ))
    }

    fun convertJsonToTodoList(json: String): ArrayList<Todo> {
        return Gson().fromJson(json, Array<Todo>::class.java).toCollection(ArrayList())
    }

    fun convertJsonToTodo(json: String): Todo {
        return Gson().fromJson(json, Todo::class.java)
    }
}