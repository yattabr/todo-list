package br.com.wobbu.todo.main

import android.graphics.Paint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.util.rangeTo
import androidx.recyclerview.widget.RecyclerView
import br.com.wobbu.todo.R
import br.com.wobbu.todo.model.Todo
import com.chauthai.swipereveallayout.ViewBinderHelper
import kotlinx.android.synthetic.main.item_todo.view.*
import java.util.*
import java.text.SimpleDateFormat
import java.time.temporal.ChronoUnit


class TodoAdapter(var activity: MainActivity) :
    RecyclerView.Adapter<TodoAdapter.CustomViewHolder>(), Filterable {

    private var items: ArrayList<Todo> = arrayListOf()
    private var filteredList: ArrayList<Todo> = arrayListOf()
    private val viewBinderHelper = ViewBinderHelper()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_todo, parent, false)
        return CustomViewHolder(activity, view)
    }

    fun createTodoList(todo: ArrayList<Todo>) {
        this.items = todo
        filteredList = todo
    }

    fun addTodo(todo: Todo) {
        filteredList.add(todo)
        notifyDataSetChanged()
    }

    fun updateTodo(todo: Todo) {
        val updatedTodo = filteredList.find { it.id == todo.id }
        filteredList[filteredList.indexOf(updatedTodo)] = todo
        viewBinderHelper.closeLayout(todo.id)
        notifyDataSetChanged()
    }

    fun deleteTodo(id: String) {
        val updatedTodo = filteredList.find { it.id == id }
        filteredList.remove(updatedTodo)
        viewBinderHelper.closeLayout(id)
        notifyDataSetChanged()
    }

    fun filterBy7Days() {
        filteredList = items
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, -7)
        val filterDate = calendar.time

        val dateFilter = filteredList.filter {
            val todoDate = SimpleDateFormat("yyyy-MM-dd").parse(it.expiry_date)
            todoDate.compareTo(filterDate) > 0
        }
        filteredList = dateFilter as ArrayList<Todo>
        notifyDataSetChanged()
    }

    fun filterBy6Months() {
        filteredList = items
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, -6)
        val filterDate = calendar.time

        val dateFilter = filteredList.filter {
            val todoDate = SimpleDateFormat("yyyy-MM-dd").parse(it.expiry_date)
            todoDate.before(filterDate)
        }
        filteredList = dateFilter as ArrayList<Todo>
        notifyDataSetChanged()
    }

    fun filterBy1Month() {
        filteredList = items
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, -1)
        val filterDate = calendar.time

        val dateFilter = filteredList.filter {
            val todoDate = SimpleDateFormat("yyyy-MM-dd").parse(it.expiry_date)
            todoDate.before(filterDate)
        }
        filteredList = dateFilter as ArrayList<Todo>
        notifyDataSetChanged()
    }

    fun cleanFilterByDate() {
        filteredList = items
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return filteredList.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        val todo = filteredList[position]
        viewBinderHelper.bind(holder.itemView.swipe_layout, todo.id)
        holder.bind(todo)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                filteredList = if (charString.isEmpty()) {
                    items
                } else {
                    val list = items.filter { item ->
                        item.name.toLowerCase()
                            .contains(charString.toLowerCase()) || item.description.toLowerCase()
                            .contains(charString.toLowerCase()) || item.status.toLowerCase()
                            .contains(charString.toLowerCase())
                    }
                    list as ArrayList<Todo>
                }
                val filterResults = FilterResults()
                filterResults.values = filteredList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredList = results!!.values as ArrayList<Todo>
                notifyDataSetChanged()
            }

        }
    }

    class CustomViewHolder(var activity: MainActivity, itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        fun bind(todo: Todo) {
            this.setIsRecyclable(false)
            itemView.cb_todo.text = todo.name
            itemView.txt_status.text = todo.status
            itemView.txt_description.text = todo.description
            itemView.txt_expire_date.text = todo.expiry_date

            itemView.cb_todo.setOnCheckedChangeListener(null)
            if (todo.status == activity.getString(R.string.completed)) {
                itemView.cb_todo.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.cb_todo.isChecked = true
            }

            itemView.bt_update.setOnClickListener {
                activity.openUpdateView(todo)
            }

            itemView.bt_delete.setOnClickListener {
                activity.openDeleteView(todo)
            }

            itemView.cb_todo.setOnCheckedChangeListener { compoundButton, b ->
                todo.status = activity.getString(R.string.completed)
                activity.completeTodo(todo)
                itemView.cb_todo.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            }
        }
    }
}