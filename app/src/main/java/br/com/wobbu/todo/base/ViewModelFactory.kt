package br.com.wobbu.todo.base

import androidx.annotation.NonNull
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import br.com.wobbu.todo.main.MainViewModel
import br.com.wobbu.todo.data.Repository
import javax.inject.Inject

open class ViewModelFactory @Inject constructor(private var repository: Repository) :
    ViewModelProvider.Factory {

    @NonNull
    override fun <T : ViewModel> create(@NonNull modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}