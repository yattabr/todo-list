package br.com.wobbu.todo.model

class Todo {
    var id: String = ""
    var name: String = ""
    var status: String = ""
    var description: String = ""
    var expiry_date: String = ""
}