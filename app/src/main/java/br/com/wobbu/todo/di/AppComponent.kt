package br.com.wobbu.todo.di

import br.com.wobbu.todo.main.MainActivity
import dagger.Component
import javax.inject.Singleton


@Component(modules = [AppModule::class, UtilsModule::class])
@Singleton
interface AppComponent {
    fun doInjection(mainActivity: MainActivity)
}
